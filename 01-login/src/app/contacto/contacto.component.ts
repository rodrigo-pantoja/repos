import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { MessageService } from './../services/message.service';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  name: string;
  email: string;
  message: string;

  constructor(public auth: AuthService, public _MessageService: MessageService) { }
  
  ngOnInit() {}

  /**
   * Process the form we have. Send to whatever backend
   * Only alerting for now
   */
  processForm() {
    const allInfo = `My name is ${this.name}. My email is ${this.email}. My message is ${this.message}`;
    alert(allInfo);
    
  }

}