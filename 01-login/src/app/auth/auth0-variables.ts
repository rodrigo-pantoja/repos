interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'Tv7S013mqUmlM6UbYo_a-LRFp_cwM7IT',
  domain: 'dev-fcjvh75o.auth0.com',
  callbackURL: 'http://localhost:3000/callback'

  
};

